# 3.15 for MSVC_RUNTIME_LIBRARY
cmake_minimum_required(VERSION 3.15)

include(cmake/helpers.cmake)

project(mh.arc VERSION 0.0.0.1 LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
option(ENABLE_CLI_BUILD "" ON)
option(ENABLE_STATIC_BUILD "" OFF)

if (ENABLE_STATIC_BUILD)
    if (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
        message(STATUS "Enabling static build")
        set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>" CACHE INTERNAL "" FORCE)
    endif()
endif()

file(GLOB_RECURSE PROJECT_SOURCES CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")
file(GLOB_RECURSE PROJECT_HEADERS CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/*.hpp")
file(GLOB_RECURSE PROJECT_UI CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/*.ui")
set(PROJECT_FILES ${PROJECT_SOURCES} ${PROJECT_HEADERS} ${PROJECT_UI})

if (NOT TARGET mh)
add_subdirectory(extern/libmh EXCLUDE_FROM_ALL)
endif()

if (NOT TARGET cxxopts)
add_subdirectory(extern/cxxopts EXCLUDE_FROM_ALL)
endif()

if (NOT TARGET nlohmann_json)
add_subdirectory(extern/json EXCLUDE_FROM_ALL)
endif()

if (NOT ENABLE_CLI_BUILD)
    set(CMAKE_AUTOUIC ON)
    set(CMAKE_AUTOMOC ON)
    set(CMAKE_AUTORCC ON)

    find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets)
    find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets)

    if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
        qt_add_executable(${PROJECT_NAME}
            MANUAL_FINALIZATION
            ${PROJECT_FILES}
        )
    else()
        add_executable(${PROJECT_NAME} ${PROJECT_FILES})
    endif()

    set_target_properties(${PROJECT_NAME} PROPERTIES
        MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
        MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
        MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
        MACOSX_BUNDLE TRUE
        WIN32_EXECUTABLE TRUE
    )
else()
    add_executable(${PROJECT_NAME} ${PROJECT_FILES})
endif()

if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(${PROJECT_NAME})
endif()

if (ENABLE_STATIC_BUILD)
    if (NOT CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
        message(STATUS "Enabling static build")
        target_link_libraries(${PROJECT_NAME} PRIVATE -static)
    endif()
endif()

target_include_directories(${PROJECT_NAME} PRIVATE src)
target_link_libraries(${PROJECT_NAME} PUBLIC mh::mh)
target_link_libraries(${PROJECT_NAME} PUBLIC cxxopts::cxxopts)
target_link_libraries(${PROJECT_NAME} PUBLIC nlohmann_json::nlohmann_json)

if (ENABLE_CLI_BUILD)
    if (WIN32 AND NOT MINGW)
        set(SUBSYSTEM_LINKER_OPTIONS "/SUBSYSTEM:CONSOLE")
    elseif (MINGW)
        set(SUBSYSTEM_LINKER_OPTIONS "-mconsole")
    endif()

    target_link_options(${PROJECT_NAME} PRIVATE ${SUBSYSTEM_LINKER_OPTIONS})
    add_definitions(-DENABLE_CLI_BUILD)
else()
    target_include_directories(${PROJECT_NAME} PRIVATE src/UI)
    target_link_libraries(${PROJECT_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Widgets)
endif()

install(TARGETS ${PROJECT_NAME}
        RUNTIME DESTINATION bin)

if (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    target_compile_definitions(${PROJECT_NAME} PRIVATE -D_SILENCE_CXX17_C_HEADER_DEPRECATION_WARNING)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -D_SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING)
endif()
        
enable_ccache()
create_target_directory_groups(${PROJECT_NAME})
