#!/bin/bash

tag_count=$(git tag --list | wc -l)
current_tag=$(git describe --abbrev=0 --tags)
previous_tag=$(git describe --abbrev=0 --tags HEAD^ || true)
 
echo "Current tag: $current_tag"
echo "Previous tag: $previous_tag"

# echo "Release notes" > CHANGELOG.md
# echo "" >> CHANGELOG.md

if [ "$tag_count" -eq 1 ]; then
    echo "Single tag"
    git log --pretty=format:"* %h - %<(80,trunc)%s <%an>" --abbrev=8 "$CI_COMMIT_TAG" >> CHANGELOG.md
else
    echo "Multiple tags"
    git log --pretty=format:"* %h - %<(80,trunc)%s <%an>" --abbrev=8 "$previous_tag".."$CI_COMMIT_TAG" >> CHANGELOG.md
fi
