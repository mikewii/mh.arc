#pragma once
#include <libmh/tools/noncopyable.hpp>
#include <libmh/tools/nonmovable.hpp>
#include <libmh/tools/arc/common.hpp>
#include <libmh/MH/Resource.hpp>

#include <filesystem>
#include <fstream>
#include <iostream>
#include <mutex>

#include <nlohmann/json.hpp>

#include "helpers.hpp"
#include "meta.hpp"

class STDFSIOstreamFun
{
private:
    std::filesystem::path m_prefix;
    std::filesystem::path m_outdir;
    nlohmann::ordered_json m_metaHeader;
    nlohmann::ordered_json::array_t m_metaArr;
    std::mutex m_metaMutex;

public:
    NONCOPYABLE(STDFSIOstreamFun)
    NONMOVABLE(STDFSIOstreamFun)

    STDFSIOstreamFun(const std::filesystem::path& arcpath,
                     const std::filesystem::path& prefix = {})
    {
        std::filesystem::path temp{arcpath};

        m_outdir.replace_filename(temp.filename());
        m_outdir.replace_extension(temp.extension().string() + ".dearc");

        if (prefix.empty())
            m_prefix = temp.replace_extension().remove_filename();
        else
            m_prefix = prefix;
    }

    auto setPrefix(const std::filesystem::path& path) noexcept -> void
    {
        m_prefix = path;
    }

    auto setMetaHeader(const mh::tools::arc::Header& header)
    {
        m_metaHeader["version"] = header.Version;
    }

    auto reserveMeta(const std::size_t size) -> void
    {
        m_metaArr.reserve(size);
    }

    auto iostreamfun(const mh::tools::arc::FileHeader& header) noexcept(false) -> std::unique_ptr<std::iostream>
    {
        std::filesystem::path fspath{onlyDir(header.getFilePathUnix())};
        std::filesystem::path path;

        path = m_prefix;
        path /= m_outdir;
        path /= fspath;

        std::error_code ec;
        std::filesystem::create_directories(path, ec);

        std::filesystem::path filename{constructFilename(header)};

        path /= filename;

        auto stream = std::make_unique<std::fstream>(  path
                                                     , std::ios::out | std::ios::trunc | std::ios::binary);

        if (!stream->is_open())
            throw std::runtime_error("Failed to open ostream: " + path.string());

        addMeta(header);

        return stream;
    }

    auto dumpMeta(const int indent = -1,
                  const std::string& name = "meta.json") -> bool
    {
        if (m_metaArr.empty())
            return false;

        std::filesystem::path path;

        path = m_prefix;
        path /= m_outdir;
        path /= name;

        std::ofstream out(path, std::ios::out | std::ios::trunc);

        if (!out.is_open())
            return false;

        nlohmann::ordered_json full;

        full["header"] = m_metaHeader;
        full["files"] = m_metaArr;

        out << full.dump(indent);

        return true;
    }

private:
    auto addMeta(const mh::tools::arc::FileHeader& header) -> void
    {
        std::lock_guard<std::mutex> lock(m_metaMutex);

        m_metaArr.push_back(meta::create(header));
    }

    auto onlyDir(const std::string& path) const noexcept -> std::filesystem::path
    {
        std::filesystem::path temp{path};

        return temp.replace_extension().remove_filename();
    }

    auto constructFilename(const mh::tools::arc::FileHeader& header) const noexcept -> std::filesystem::path
    {
        std::string extention{getExtention(header.ResourceHash)};
        std::filesystem::path temp{header.getFilePathUnix()};
        std::filesystem::path filename;

        filename.replace_filename(temp.filename());
        filename.replace_extension(temp.extension().string() + extention);

        return filename;
    }
};
