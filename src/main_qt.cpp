#if !defined(ENABLE_CLI_BUILD)
#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    QCommandLineParser parser;
    bool parsed = false;

    parser.addHelpOption();
    parser.addOptions({
                       {{"i", "input"}, "Input .arc quest file", "filename"},
                       {{"o", "output"}, "Output .arc quest file", "filename"},
                       });



    if (parsed)
        return 0;

    w.show();

    return app.exec();
}
#endif
