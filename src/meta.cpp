#include "meta.hpp"
#include "helpers.hpp"
#include <nlohmann/json.hpp>
#include <filesystem>

namespace meta {

auto create(const mh::tools::arc::FileHeader& header) noexcept -> nlohmann::ordered_json
{
    nlohmann::ordered_json meta;
    std::filesystem::path filepath{header.getFilePathUnix()};

    meta["filepath"] = filepath.replace_extension(getExtention(header.ResourceHash));
    meta["resource"] = header.ResourceHash;
    meta["flags"] = header.getFlags();

    return meta;
}

auto validate(const nlohmann::ordered_json& meta) noexcept -> bool
{
    if (!meta.is_object())
        return false;

    if (!meta.contains("filepath") || !meta["filepath"].is_string())
        return false;

    if (!meta.contains("resource") || !meta["resource"].is_number())
        return false;

    if (!meta.contains("flags") || !meta["flags"].is_number())
        return false;

//    if (meta["resource"] == 0)
//        return false;

    std::filesystem::path path = meta["filepath"];

    path.replace_extension();

    if (path.string().size() >= mh::tools::arc::FileHeader::ARC_FILEPATH_MAX)
        return false;

    return true;
}

}; /// namespace meta
