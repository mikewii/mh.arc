#pragma once
#include <nlohmann/json_fwd.hpp>
#include <libmh/tools/arc/common.hpp>

namespace meta {

extern auto create(const mh::tools::arc::FileHeader& header) noexcept -> nlohmann::ordered_json;
extern auto validate(const nlohmann::ordered_json& meta) noexcept -> bool;

}; /// namespace meta
