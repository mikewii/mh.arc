#pragma once
#include <filesystem>
#include <libmh/tools/arc/common.hpp>

struct RepackSettings {
    mh::tools::arc::sVersion::e version;
    int compressionLevel;
    int compressionStrategy;
    bool isBE;

    RepackSettings() = default;
    RepackSettings(const mh::tools::arc::sVersion::e ver,
                   const int level,
                   const int strategy,
                   const bool be) noexcept
        : version{ver}
        , compressionLevel{level}
        , compressionStrategy{strategy}
        , isBE{be}
    {
    }

    auto isValid(void) const noexcept -> bool;
};

extern auto repack(const std::filesystem::path& in,
                   std::filesystem::path out,
                   RepackSettings settings,
                   const bool async) -> bool;

extern auto recurseRepack(const std::filesystem::path& in,
                          std::filesystem::path out,
                          RepackSettings settings,
                          const bool async) -> bool;
