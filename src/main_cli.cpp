#if defined(ENABLE_CLI_BUILD)
#include <cxxopts.hpp>
#include <iostream>
#include "helpers.hpp"
#include "unpack.hpp"
#include "repack.hpp"
#include <zlib.h>

auto main(int argc, char* argv[]) -> int
{
    cxxopts::Options options("mh.arc", "Unpack/Repack .arc archive files");
    std::string inPath, outPath;
    bool    fCreate{false};
    bool    fUnpack{true};
    bool    fRecurse{false};
    bool    fNoAsync{false};
    int     fIndent{4};
    int     fVersion = mh::tools::arc::sVersion::e::MH4U_MHXX_MHGU;
    int     fCompressionLevel = Z_DEFAULT_COMPRESSION;
    int     fCompressionStrategy = Z_DEFAULT_STRATEGY;
    bool    fIsBE{false};

    options.add_options()
        ("c,create", "Create .arc from input path", cxxopts::value<bool>(fCreate))
        ("u,unpack", "Unpack .arc from input path", cxxopts::value<bool>(fUnpack)->default_value("true"))

        ("i,input", "Input path", cxxopts::value<std::string>(inPath))
        ("o,output", "Output path", cxxopts::value<std::string>(outPath))

        ("r,recurse", "Recursively unpack .arc from input path", cxxopts::value<bool>(fRecurse))

        ("n,noasync", "Dont use async operations", cxxopts::value<bool>(fNoAsync)->default_value("false"))
        ("m,mindent", "Metadata JSON indentation, -1 for compact mode", cxxopts::value<int>(fIndent)->default_value("4"))

        ("v,version", "ARC version", cxxopts::value<int>(fVersion)->default_value("17"))
        ("l,level", "Compression level -1 - 9", cxxopts::value<int>(fCompressionLevel)->default_value("-1"))
        ("s,strategy", "Compression strategy 0 - 4", cxxopts::value<int>(fCompressionStrategy)->default_value("0"))
        ("b,bigendian", "Create big endian ARC", cxxopts::value<bool>(fIsBE)->default_value("false"))

        ("h,help", "Print help and exit.");

    try {
        auto result = options.parse(argc, argv);

        if (result.count("help")) {
            std::cout << options.help() << std::endl;
            std::exit(0);
        }
    } catch (const std::exception& e) {
        std::cerr << "Error parsing options: " << e.what() << std::endl;
        return 1;
    }

    std::filesystem::path in{inPath}, out{outPath};

    if (!pathExist(in))
        return 1;

    if (fCreate) {
        std::cout << ":: Create mode ::" << std::endl;

        RepackSettings settings(  static_cast<mh::tools::arc::sVersion::e>(fVersion)
                                , fCompressionLevel
                                , fCompressionStrategy
                                , fIsBE);

        if (!settings.isValid())
            return 1;

        try {
            if (fRecurse)
                (void)recurseRepack(in, out, settings, !fNoAsync);
            else
                (void)repack(in, out, settings, !fNoAsync);
        } catch (const std::exception& e) {
            std::cerr << e.what() << std::endl;
        }
    } else if (fUnpack) {
        std::cout << ":: Unpack mode ::" << std::endl;

        if (!out.empty()) {
            if (!pathExist(out))
                return 1;

            if (!std::filesystem::is_directory(out)) {
                std::cerr << "Output path must be directory: " << out.string() << std::endl;
                return 1;
            }
        }

        try {
            if (fRecurse)
                (void)recurseUnpack(in, out, !fNoAsync, fIndent);
            else
                (void)unpack(in, out, !fNoAsync, fIndent);
        } catch (const std::exception& e) {
            std::cerr << e.what() << std::endl;
        }
    }

    return 0;
}
#endif
