#include "repack.hpp"
#include "stdfsistreamfun.hpp"
#include <libmh/tools/arc/writer.hpp>
#include "helpers.hpp"

auto repack(const std::filesystem::path& in,
            std::filesystem::path out,
            RepackSettings settings,
            const bool async) -> bool
{
    if (!std::filesystem::is_directory(in))
        throw std::runtime_error("Input path must be directory: " + in.string());

    if (out.empty())
        out = relativePath(in);

    mh::tools::arc::Writer writer(out.string());

    if (!writer.isOStreamOpen())
        throw std::runtime_error("Failed to open output path: " + out.string());

    STDFSIstreamFun fun(  in.string()
                        , settings.compressionLevel
                        , settings.compressionStrategy);

    if (!fun.loadMeta())
        throw std::runtime_error("Failed to load meta");

    if (fun.getVersion(settings.version))
        std::cout << "Using version from meta" << std::endl;

    fun.setCompressionLevel(settings.compressionLevel);
    fun.setCompressionStrategy(settings.compressionStrategy);

    auto istreamfun = std::bind(  &STDFSIstreamFun::istreamfun
                                , std::ref(fun)
                                , std::placeholders::_1);

    if (async) {
        if (!writer.compress_async(  fun.filesNum()
                                   , settings.version
                                   , istreamfun
                                   , settings.isBE))
            throw std::runtime_error("Failed to async compress: " + in.string());
    } else {
        if (!writer.compress(  fun.filesNum()
                             , settings.version
                             , istreamfun
                             , settings.isBE))
            throw std::runtime_error("Failed to compress: " + in.string());
    }

    return true;
}

auto recurseRepack(const std::filesystem::path& in,
                   std::filesystem::path out,
                   RepackSettings settings,
                   const bool async) -> bool
{
    return true;
}

auto RepackSettings::isValid(void) const noexcept -> bool
{
    if (!mh::tools::arc::sVersion::contains(version)) {
        std::cerr << "Invalid version: "
                  << version
                  << std::endl;

        return false;
    }

    if (   compressionLevel < Z_DEFAULT_COMPRESSION
        || compressionLevel > Z_BEST_COMPRESSION) {
        std::cerr << "Invalid compression level: "
                  << compressionLevel
                  << std::endl;

        return false;
    }

    if (   compressionStrategy < Z_DEFAULT_STRATEGY
        || compressionStrategy > Z_FIXED) {
        std::cerr << "Invalid compression strategy: "
                  << compressionStrategy
                  << std::endl;

        return false;
    }

    return true;
}
