#include "unpack.hpp"
#include "stdfsiostreamfun.hpp"
#include <libmh/tools/arc/reader.hpp>

auto unpack(const std::filesystem::path& in,
            const std::filesystem::path& out,
            const bool async,
            const int indent) -> bool
{
    if (!std::filesystem::is_regular_file(in))
        throw std::runtime_error("Input path must be file: " + in.string());

    mh::tools::arc::Reader reader(in.string());
    STDFSIOstreamFun fun(in);

    auto iostreamfun = std::bind(  &STDFSIOstreamFun::iostreamfun
                                 , std::ref(fun)
                                 , std::placeholders::_1);

    if (!out.empty())
        fun.setPrefix(out);

    if (!reader.read()) {
        std::cerr << "Failed to read arc: " << in << std::endl;
        return false;
    }

    fun.reserveMeta(reader.getFileHeaders().size());
    fun.setMetaHeader(reader.getHeader());

    if (async) {
        if (!reader.uncompress_all_async(in.string(), iostreamfun))
            throw std::runtime_error("Failed to async uncompress: " + in.string());
    } else {
        if (!reader.uncompress_all(iostreamfun))
            throw std::runtime_error("Failed to uncompress: " + in.string());
    }

    return fun.dumpMeta(indent);
}

auto recurseUnpack(const std::filesystem::path& in,
                   const std::filesystem::path& out,
                   const bool async,
                   const int indent) -> bool
{
    if (!std::filesystem::is_directory(in))
        throw std::runtime_error("Input path must be directory: " + in.string());

    auto perm = std::filesystem::directory_options::skip_permission_denied;
    std::filesystem::recursive_directory_iterator it(in, perm);
    bool good = true;

    for (const auto& entry : it) {
        if (!entry.is_regular_file())
            continue;

        try {
            good &= unpack(entry.path(), out, async, indent);
        } catch (const std::exception& e) {
            std::cerr << e.what() << std::endl;
        }
    }

    return good;
}
