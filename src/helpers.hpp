#pragma once
#include <filesystem>
#include <iostream>
#include <libmh/types.hpp>

extern auto getExtention(const u32 hash) noexcept -> std::string;
extern auto pathExist(const std::filesystem::path& path) noexcept -> bool;
extern auto relativePath(std::filesystem::path path) noexcept -> std::filesystem::path;
