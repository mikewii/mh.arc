#include "helpers.hpp"
#include <algorithm>
#include <string> // std::to_string
#include <libmh/MH/Resource.hpp>

auto pathExist(const std::filesystem::path& path) noexcept -> bool
{
    std::error_code ec;

    if (!std::filesystem::exists(path, ec)) {
        std::cerr << "Path does not exist: " << path << std::endl;

        return false;
    }

    if (ec)
        std::cerr << ec.message() << std::endl;

    return true;
}

auto getExtention(const u32 hash) noexcept -> std::string
{
    std::string extention;
    const char* str = mh::sResource::getExtention(hash);

    if (str)
        extention = str;
    else
        extention = std::to_string(hash);

    std::transform(  extention.begin()
                   , extention.end()
                   , extention.begin()
                   , [](unsigned char c){ return std::tolower(c); });

    return extention;
}

static auto sanitizeFilename(std::string filename) -> std::string
{
    static std::string arc = ".arc";
    std::string::size_type pos = std::string::npos;

    pos = filename.rfind(arc);

    while (pos != std::string::npos) {
        filename.erase(pos, arc.size());

        pos = filename.rfind(arc);
    }

    return filename;
}

auto relativePath(std::filesystem::path path) noexcept -> std::filesystem::path
{
    auto filename = sanitizeFilename(path.filename().string());

    path.replace_filename(std::move(filename));
    path.replace_extension(".arc");

    return path;
}
