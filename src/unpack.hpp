#pragma once
#include <filesystem>

extern auto unpack(const std::filesystem::path& in,
                   const std::filesystem::path& out,
                   const bool async,
                   const int indent) -> bool;

extern auto recurseUnpack(const std::filesystem::path& in,
                          const std::filesystem::path& out,
                          const bool async,
                          const int indent) -> bool;
