#pragma once
#include "meta.hpp"
#include <libmh/tools/noncopyable.hpp>
#include <libmh/tools/nonmovable.hpp>
#include <libmh/tools/arc/common.hpp>
#include <libmh/tools/arc/writer.hpp>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

class STDFSIstreamFun
{
private:
    std::string m_metaName = "meta.json";
    std::vector<std::filesystem::path> m_files;
    std::filesystem::path m_prefix;
    std::filesystem::path m_dirpath;
    nlohmann::ordered_json m_meta;
    int m_compressionLevel;
    int m_compressionStrategy;

public:
    NONCOPYABLE(STDFSIstreamFun)
    NONMOVABLE(STDFSIstreamFun)

    STDFSIstreamFun(const std::string& dirpath,
                    const int level,
                    const int strategy)
        : m_dirpath{dirpath}
        , m_compressionLevel{level}
        , m_compressionStrategy{strategy}
    {
    }

    auto setCompressionLevel(const int level) noexcept -> void
    {
        m_compressionLevel = level;
    }

    auto setCompressionStrategy(const int strategy) noexcept -> void
    {
        m_compressionStrategy = strategy;
    }

    auto filesNum(void) const noexcept -> u16
    {
        // cant be higher than u16
        return static_cast<u16>(m_files.size());
    }

    auto getVersion(mh::tools::arc::sVersion::e& version) -> bool
    {
        if (!m_meta.contains("header") || !m_meta["header"].is_object())
            return false;

        if (!m_meta["header"].contains("version") || !m_meta["header"]["version"].is_number())
            return false;

        auto ver = m_meta["header"]["version"];

        if (!mh::tools::arc::sVersion::contains(ver))
            return false;

        version = ver;

        return true;
    }

    auto loadMeta(void) -> bool
    {
        std::ifstream meta(m_dirpath / m_metaName);

        if (!meta.is_open())
            return false;

        m_meta = nlohmann::ordered_json::parse(meta);

        if (   m_meta.empty()
            || !validateMeta())
            return false;

        buildFileVectorFromMeta();

        return true;
    }

    auto validateMeta(void) const -> bool
    {
        if (!m_meta.contains("files") || !m_meta["files"].is_array())
            return false;

        std::error_code ec;
        const nlohmann::ordered_json::array_t& files = m_meta["files"];

        for (const auto& meta : files)
            if (!meta::validate(meta))
                return false;

        // check paths
        for (const auto& meta : files) {
            if (!std::filesystem::exists(  m_dirpath / meta["filepath"]
                                         , ec))
                return false;

            if (ec)
                std::cerr << ec.message() << std::endl;
        }

        return true;
    }

    auto buildFileVectorFromMeta(void) -> bool
    {
        std::error_code ec;
        const nlohmann::ordered_json::array_t& files = m_meta["files"];

        for (const auto& meta : files)
            m_files.push_back(m_dirpath / meta["filepath"]);

        return m_files.size() == files.size();
    }

    auto istreamfun(const u16 id) noexcept(false) -> mh::tools::arc::sWriteOp
    {
        if (id >= m_files.size())
            throw std::runtime_error(  "Bad id: "
                                     + std::to_string(id)
                                     + " / "
                                     + std::to_string(m_files.size()));

        bool pass = false;
        auto pathWE = fixPath(m_files[id]);
        auto pathE = fixPath(m_files[id], false);
        mh::tools::arc::sWriteOp op;
        auto in = std::make_unique<std::ifstream>( m_files[id]
                                                  , std::ios::in | std::ios::binary);

        if (!in->is_open())
            throw std::runtime_error("Failed to open ifstream: " + m_files[id].string());

        if (!op.header.setFilePath(pathWE.string()))
            throw std::runtime_error("Failed to set path: " + m_files[id].string());

        for (const auto& meta : m_meta["files"]) {
            if (static_cast<const std::string&>(meta["filepath"]) == pathE) {
                std::error_code ec;
                auto uncompressedSize = std::filesystem::file_size(  m_files[id]
                                                                   , ec);

                if (ec)
                    std::cerr << ec.message() << std::endl;

                if (uncompressedSize == 0)
                    throw std::runtime_error("Incorrect file size");

                op.header.Flags = static_cast<u32>(meta["flags"]) & 7;
                op.header.UncompressedSize = uncompressedSize & 0x1FFFFFFF;
                op.header.ResourceHash = meta["resource"];

                pass = true;
                break;
            }
        }

        if (!pass)
            throw std::runtime_error("Not found: " + pathE.string());

        op.op = mh::tools::arc::WriteOp::COMPRESS;
        op.istream = std::move(in);
        op.level = m_compressionLevel;
        op.strategy = m_compressionStrategy;

        return op;
    }

private:
    auto fixPath(std::filesystem::path path,
                 const bool removeExtention = true) -> std::filesystem::path
    {
        if (removeExtention)
            path.replace_extension();

        path = std::filesystem::relative(path, m_dirpath);

        return path;
    }
};
